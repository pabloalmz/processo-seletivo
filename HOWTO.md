Na primeira parte, interliguei a IDE Eclipse vers�o 2018-12 ao banco de dados MYSQLServer 5.7, ao qual, foi poss�vel conectar o banco de dados ao Eclipse, desta forma, foi poss�vel iniciar os procedimentos necess�rios para o desenvolvimento do projeto.
Posteriormente, configurei o programa Tomcat 8.5 para criar o servi�o web em minha m�quina (localhost) dispondo da porta 8081, ao qual, efetuei a sua conex�o dentro do IDE Eclipse. 
Na segunda parte, realizei um fork do reposit�rio do GitLab para a IDE Eclipse onde dei in�cio ao processo de cria��o do projeto em conson�ncia com os requisitos.
Destarte, foi necess�rio alterar os pacotes referentes ao ProdutoService.java e o pacote DAO. No produto servisse foram implementados os servi�os (API  REST) expondo m�todos para realiza��o do CRUD.
Utilizando o Vue.JS, criei o Cadastro de Produtos em Javascript que cont�m as fun��es de criar e alterar produtos, criei na index.js a fun��o para buscar e excluir os produtos concatenados com as respectivas fun��es no Axios: POST, UPDATE, PUT E DELETE no ProdutoService.java.
Interliguei no procedimento de cria��o, um formul�rio em /novo-produto.html utilizando Vue.js, ao qual, faz uma conex�o com o Java e envia para o banco de dados as informa��es postadas.
Utilizei o �cone criado para realizar a fun��o deletar promovendo o reload na p�gina automaticamente por meio do click.
Por fim, consegui atingir o objetivo de realizar um CRUD utilizando as plataformas requisitadas.
